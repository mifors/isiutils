//
//  UIImage+ISI.m
//  Eureka inTouch
//
//  Created by Ivan Isaev on 13.06.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "UIImage+ISI.h"

@implementation UIImage (ISI)

+ (UIImage *)isi_imageByColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)isi_scaleToSize:(CGSize)newSize
{
    CGRect thumbnailRect = [self isi_thumbnailRectWithSourceSize:self.size targetSize:newSize];
    UIGraphicsBeginImageContext(newSize);
    [self drawInRect:thumbnailRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (CGRect)isi_thumbnailRectWithSourceSize:(CGSize)sourceSize targetSize:(CGSize)targetSize
{
    CGFloat widthFactor = targetSize.width / sourceSize.width;
    CGFloat heightFactor = targetSize.height / sourceSize.height;
    CGFloat scaleFactor = (widthFactor > heightFactor) ? widthFactor : heightFactor;
    
    CGFloat scaledWidth  = sourceSize.width * scaleFactor;
    CGFloat scaledHeight = sourceSize.height * scaleFactor;
    
    CGPoint thumbnailPoint = CGPointZero;
    
    if (widthFactor > heightFactor) {
        thumbnailPoint.y = (targetSize.height - scaledHeight) * 0.5;
    } else if (widthFactor < heightFactor) {
        thumbnailPoint.x = (targetSize.width - scaledWidth) * 0.5;
    }
    return CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight);
}

@end
