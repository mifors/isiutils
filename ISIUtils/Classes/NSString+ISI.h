//
//  NSString+ISI.h
//  triviabox
//
//  Created by Ivan Isaev on 16.03.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ISI)

- (BOOL)isi_isEmpty;
- (BOOL)isi_hasData;

- (NSString *)isi_trim;
- (NSArray *)isi_split:(NSString *)separator;
- (NSArray *)isi_splitBySeparators:(NSString *)separators;
- (NSArray *)isi_splitByCharacterSet:(NSCharacterSet *)characterSet;
- (NSArray *)isi_splitAndTrim:(NSString *)separator;

- (BOOL)isi_isNumeric;
- (BOOL)isi_isEqualIgnoreCase:(NSString *)otherString;

@end
