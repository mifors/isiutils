//
//  UIColor+ISI.h
//  triviabox
//
//  Created by Ivan Isaev on 16.03.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ISI)

+ (UIColor *)isi_colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b;
+ (UIColor *)isi_colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b A:(float)a;
+ (UIColor *)isi_colorWithHex:(NSString *)hexString;

+ (UIImage *)isi_gradientImageWithTopColor:(UIColor *)top bottomColor:(UIColor *)bottom height:(CGFloat)height;
+ (UIColor *)isi_gradientWithTopColor:(UIColor *)top bottomColor:(UIColor *)bottom height:(CGFloat)height;

- (NSArray *)isi_rgbaComponents;

@end

UIKIT_STATIC_INLINE UIColor * isi_RGB(int red, int green, int blue)
{
    return [UIColor isi_colorWithR:red G:green B:blue];
}

UIKIT_STATIC_INLINE UIColor * isi_RGBA(int red, int green, int blue, float alpha)
{
    return [UIColor isi_colorWithR:red G:green B:blue A:alpha];
}

UIKIT_STATIC_INLINE UIColor * isi_hexColor(NSString *hex)
{
    return [UIColor isi_colorWithHex:hex];
}
