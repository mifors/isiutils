//
//  UIImage+ISI.h
//  Eureka inTouch
//
//  Created by Ivan Isaev on 13.06.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ISI)

+ (UIImage *)isi_imageByColor:(UIColor *)color;
- (UIImage *)isi_scaleToSize:(CGSize)newSize;

@end
