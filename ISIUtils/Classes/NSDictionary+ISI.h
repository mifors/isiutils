//
//  NSDictionary+ISI.h
//  Eureka inTouch
//
//  Created by Ivan Isaev on 07.04.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (ISI)

- (NSNumber *)numberForKey:(id)aKey;
- (NSString *)stringForKey:(id)aKey;
- (NSArray *)arrayForKey:(id)aKey;

@end
