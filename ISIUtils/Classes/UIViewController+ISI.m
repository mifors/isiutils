//
//  UIViewController+ISI.m
//  ISIUtils
//
//  Created by Ivan Isaev on 01.09.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "UIViewController+ISI.h"

@implementation UIViewController (ISI)

- (void)isi_showInfoMessage:(NSString *)message
{
    [self isi_showInfoMessage:message withTitle:nil closeButton:nil];
}

- (void)isi_showInfoMessage:(NSString *)message withTitle:(NSString *)title
{
    [self isi_showInfoMessage:message withTitle:title closeButton:nil];
}

- (void)isi_showInfoMessage:(NSString *)message withTitle:(NSString *)title closeButton:(NSString *)closeButton
{
    NSString *closeText = closeButton.length ? closeButton : @"OK";
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:closeText style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
