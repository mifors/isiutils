//
//  NSString+ISI.m
//  triviabox
//
//  Created by Ivan Isaev on 16.03.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "NSString+ISI.h"

@implementation NSString (ISI)

/**
 Сначала удаляются пробельные и NewLine символы, затем длина строки сравнивается с 0.
 */
- (BOOL)isi_isEmpty
{
    return [[self isi_trim] length] == 0;
}

/**
 Метод, обратный методу isi_isEmpty
 */
- (BOOL)isi_hasData
{
    return ![self isi_isEmpty];
}

/**
 Удаление пробелов и символов перевода строки в начале и конце строки
 */
- (NSString *)isi_trim
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

/**
 Разбиение строки на элементы по символу, переданному в separator.
 В результирующий массив складываются только не "пустые" элементы,
 для этого каждый полученный элемент обрабатывается методом isi_trim.
 */
- (NSArray *)isi_split:(NSString *)separator
{
    NSArray *elements = [self componentsSeparatedByString:separator];
    NSMutableArray *result = [NSMutableArray array];
    for (NSString *element in elements) {
        if ([element isi_hasData]) {
            [result addObject:element];
        }
    }
    return result;
}

/**
 По переданной в строке separators символам создается CharacterSet
 далее разбиение аналогично методу isi_splitByCharacterSet
 */
- (NSArray *)isi_splitBySeparators:(NSString *)separators
{
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:separators];
    return [self isi_splitByCharacterSet:characterSet];
}

/**
 
 */
- (NSArray *)isi_splitByCharacterSet:(NSCharacterSet *)characterSet
{
    NSArray *elements = [self componentsSeparatedByCharactersInSet:characterSet];
    NSMutableArray *result = [NSMutableArray array];
    for (NSString *element in elements) {
        if ([element isi_hasData]) {
            [result addObject:element];
        }
    }
    return result;
}

/**
 
 */
- (NSArray *)isi_splitAndTrim:(NSString *)separator
{
    NSMutableArray *values = [[self isi_split:separator] mutableCopy];
    for (int i = 0; i < values.count; i++) {
        values[i] = [values[i] isi_trim];
    }
    return values;
}

/**
 Проверяет, является ли строка числом
 */
- (BOOL)isi_isNumeric
{
    NSRange r = [self rangeOfString:@"^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$" options:NSRegularExpressionSearch];
    return r.location != NSNotFound;
}

/**
 Сравнивает строки без учета регистра
 */
- (BOOL)isi_isEqualIgnoreCase:(NSString *)otherString
{
    return [self caseInsensitiveCompare:otherString] == NSOrderedSame;
}

@end
