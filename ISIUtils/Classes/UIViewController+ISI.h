//
//  UIViewController+ISI.h
//  ISIUtils
//
//  Created by Ivan Isaev on 01.09.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ISI)

- (void)isi_showInfoMessage:(NSString *)message;
- (void)isi_showInfoMessage:(NSString *)message withTitle:(NSString *)title;
- (void)isi_showInfoMessage:(NSString *)message withTitle:(NSString *)title closeButton:(NSString *)closeButton;

@end
