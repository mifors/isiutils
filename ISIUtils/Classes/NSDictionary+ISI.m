//
//  NSDictionary+ISI.m
//  Eureka inTouch
//
//  Created by Ivan Isaev on 07.04.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "NSDictionary+ISI.h"

@implementation NSDictionary (ISI)

- (NSNumber *)numberForKey:(id)aKey
{
    NSInteger num = [self[aKey] integerValue];
    return @(num);
}

- (NSString *)stringForKey:(id)aKey
{
    return [NSString stringWithFormat:@"%@", self[aKey]];
}

- (NSArray *)arrayForKey:(id)aKey
{
    id list = self[aKey];
    if (list) {
        return [list isKindOfClass:[NSArray class]] ? list : @[list];
    }
    return @[];
}

@end
