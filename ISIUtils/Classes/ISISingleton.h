//
//  ISISingleton.h
//
//  Created by Ivan Isaev on 12.04.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#define ISI_DEFAULT_SHARED_INSTANCE_METHOD      isi_sharedInstance

/**
 Добавление методов в интерфейсе класса
 */
#define ISI_SINGLETON_DECLARE_DEFAULT           ISI_SINGLETON_DECLARE(ISI_DEFAULT_SHARED_INSTANCE_METHOD)
#define ISI_SINGLETON_DECLARE(ShareMethod)      + (instancetype)ShareMethod;


/**
 Генерация кода синглтона
 */
#define ISI_SINGLETON_SYNTHESIZE_DEFAULT                                        ISI_SINGLETON_SYNTHESIZE_WITH_CONFIGURATION_METHOD(isi_emptyConfiguration)
#define ISI_SINGLETON_SYNTHESIZE_WITH_CONFIGURATION_METHOD(ConfigureMethod)     ISI_SINGLETON_SYNTHESIZE(ISI_DEFAULT_SHARED_INSTANCE_METHOD,ConfigureMethod)
#define ISI_SINGLETON_SYNTHESIZE(ShareMethod,ConfigureMethod)   \
\
+ (instancetype)ShareMethod \
{\
    static dispatch_once_t onceToken;\
    static id _sharedInstance;\
\
    dispatch_once(&onceToken, ^{\
        _sharedInstance = [[super allocWithZone:NULL] init];\
        if (_sharedInstance) {\
            [_sharedInstance isi_configureSingleton];\
        }\
    });\
\
    return _sharedInstance;\
}\
\
- (void)isi_configureSingleton\
{\
    SEL selector = @selector(ConfigureMethod);\
    if (selector && [self respondsToSelector:selector]) {\
        [self ConfigureMethod];\
    }\
}\
\
- (void)isi_emptyConfiguration {}\
+ (id)allocWithZone:(NSZone *)zone  { return [self ShareMethod]; }\
- (id)copyWithZone:(NSZone *)zone   { return self; }\
- (id)init  { return self; }


