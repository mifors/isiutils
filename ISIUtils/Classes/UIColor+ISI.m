//
//  UIColor+ISI.m
//  triviabox
//
//  Created by Ivan Isaev on 16.03.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "UIColor+ISI.h"
#import "NSString+ISI.h"

@implementation UIColor (ISI)

+ (UIColor *)isi_colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b
{
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}

+ (UIColor *)isi_colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b A:(float)a
{
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:a];
}

+ (UIColor *)isi_colorWithHex:(NSString *)hexString
{
    NSString *hex = [[hexString isi_trim] stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    BOOL withAlpha = hex.length == 4 || hex.length == 8;
    BOOL compact = hex.length == 3 || hex.length == 4;
    
    if (hex.length == 6 || compact || withAlpha) {
        NSScanner *scanner = [NSScanner scannerWithString:hex];
        unsigned int cc = 0;
        unsigned int alphaOffset = withAlpha ? (compact ? 4 : 8) : 0;
        unsigned int channelOffset = compact ? 4 : 8;
        unsigned int mask = compact ? 0x0F : 0xFF;
        unsigned int multiplier = 0xFF/mask;
        
        if ([scanner scanHexInt:&cc]) {
            NSInteger r = ((cc >> (2 * channelOffset + alphaOffset)) & mask) * multiplier;
            NSInteger g = ((cc >> (channelOffset + alphaOffset)) & mask) * multiplier;
            NSInteger b = ((cc >> alphaOffset) & mask) * multiplier;
            
            float a = withAlpha ? ((cc & mask) / (float)mask) : 1.f;
            return [self isi_colorWithR:r G:g B:b A:a];
        }
    }
    return [UIColor magentaColor];
}

+ (UIImage *)isi_gradientImageWithTopColor:(UIColor *)top bottomColor:(UIColor *)bottom height:(CGFloat)height
{
    CGSize size = CGSizeMake(1, height);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat locations[2] = {0.0, 1.0};
    CGFloat components[8];
    
    NSArray *colorComponents = [top isi_rgbaComponents];
    for (NSUInteger i = 0; i < colorComponents.count; i++) {
        components[i] = [colorComponents[i] floatValue];
    }
    
    colorComponents = [bottom isi_rgbaComponents];
    for (NSUInteger i = 0; i < colorComponents.count; i++) {
        components[i + 4] = [colorComponents[i] floatValue];
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, components, locations, 2);
    
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, size.height), 0);
    
    CGColorSpaceRelease(colorSpace);
    CGGradientRelease(gradient);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIColor *)isi_gradientWithTopColor:(UIColor *)top bottomColor:(UIColor *)bottom height:(CGFloat)height
{
    return [UIColor colorWithPatternImage:[UIColor isi_gradientImageWithTopColor:top bottomColor:bottom height:height]];
}

- (NSArray *)isi_rgbaComponents
{
    CGFloat r,g,b,a;
    if (![self getRed:&r green:&g blue:&b alpha:&a]) {
        if ([self getWhite:&r alpha:&a]) { // Grayscale
            return @[@(r), @(r), @(r), @(a)];
        }
    }
    return @[@(r), @(g), @(b), @(a)];
}

@end
