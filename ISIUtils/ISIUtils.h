//
//  ISIUtils.h
//  ISIUtils
//
//  Created by Ivan Isaev on 01.08.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


//! Project version number for ISIUtils.
FOUNDATION_EXPORT double ISIUtilsVersionNumber;

//! Project version string for ISIUtils.
FOUNDATION_EXPORT const unsigned char ISIUtilsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ISIUtils/PublicHeader.h>

#ifndef ISIUtils_ISIDefines_h
#define ISIUtils_ISIDefines_h

#define ISI_LocaleRU        [NSLocale localeWithLocaleIdentifier:@"ru_RU"];

#define ISI_AppDelegate             ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define ISI_UserDefaults            [NSUserDefaults standardUserDefaults]
#define ISI_SharedApplication       [UIApplication sharedApplication]
#define ISI_MainBundle              [NSBundle mainBundle]
#define ISI_MainScreen              [UIScreen mainScreen]
#define ISI_NotificationCenter      [NSNotificationCenter defaultCenter]

#define ISI_ShowInfoMessage(t,m)  [[[UIAlertView alloc] initWithTitle:t message:m delegate:nil cancelButtonTitle:@"Ок" otherButtonTitles:nil] show]

#define ISI_SystemVersion       [[[UIDevice currentDevice] systemVersion] floatValue]
#define ISI_UUID                [[UIDevice currentDevice] identifierForVendor].UUIDString

#define ISI_SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define ISI_SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define ISI_SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define ISI_SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define ISI_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define ISI_ScreenWidth         [UIScreen mainScreen].bounds.size.width
#define ISI_ScreenHeight        [UIScreen mainScreen].bounds.size.height

#define ISI_is_iPad         (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define ISI_is_iPhone4      (!ISI_is_iPad && MAX(ISI_ScreenWidth,ISI_ScreenHeight) == 480)
#define ISI_is_iphone5      (!ISI_is_iPad && MAX(ISI_ScreenWidth,ISI_ScreenHeight) == 568)
#define ISI_is_iphone6      (!ISI_is_iPad && MAX(ISI_ScreenWidth,ISI_ScreenHeight) == 667)
#define ISI_is_iphone6P     (!ISI_is_iPad && MAX(ISI_ScreenWidth,ISI_ScreenHeight) == 736)

/**
 Возвращает UIViewController, находящийся в данный момент на экране
 */
extern UIViewController *isi_visibleViewController();

/**
 Генерирует новый UUID заданной длины < 32 (подстрока от стандартного UUID)
 @return UUID
 */
NS_INLINE NSString *isi_createShortUID(int length)
{
    CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
    CFRelease(newUniqueId);
    
    uuidString = [uuidString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if (length == 0 || length > uuidString.length) {
        return uuidString;
    }
    return [uuidString substringToIndex:length];
}

/**
 Генерирует новый UUID
 @return UUID
 */
NS_INLINE NSString *isi_createUUID()
{
    CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
    CFRelease(newUniqueId);
    return uuidString;
}

#endif
